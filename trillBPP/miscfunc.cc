/*
 * miscfunc.cc
 *
 *  Created on: 29-Sep-2017
 *      Author: shivujagga
 */


#include <trillBPP/miscfunc.h>

namespace trill {


std::complex<double> elem_mult_sum_complex(const cvec &m1, const cvec &m2)
{
  it_assert_debug((m1.size() == m2.size() ), "elem_mult_sum_complex : size not same");
  return trill::dot(conj(m1),m2);
}

//-----------CMAT operations
	cmat operator+(const double &s, const cmat &m)
	{
	  it_assert_debug(m.rows() > 0 && m.cols() > 0, "operator+(): Matrix of zero length");
	  cmat temp = m;
	  for (int i = 0;i < m._datasize();i++) {
		temp._data()[i] += s;
	  }
	  return temp;
	}

	cmat operator/(const cmat &m, const double &s)
	{
	  it_assert_debug(m.rows() > 0 && m.cols() > 0, "operator/(): Matrix of zero length");
	  cmat temp = m;
	  for (int i = 0;i < m._datasize();i++) {
		temp._data()[i] /= (double)s;
	  }
	  return temp;
	}

//-----Convert anything to string
	template <typename T>
	std::string to_str(const T &i)
	{
	  std::ostringstream ss;
	  //ss.precision(8);	//change to (scientific,floatfield) & uncomment this to get scientific notation
	  ss.setf(std::ostringstream::dec, std::ostringstream::basefield);
	  ss << i;
	  return ss.str();
	}
	template std::string to_str(const cvec &v);
	template std::string to_str(const vec &v);

//--------REverse the vector
	template<class T>
	Vec<T> reverse(const Vec<T> &in) {
	  int i, s = in.length();
	  Vec<T> out(s);
	  for (i = 0;i < s;i++)
		out[i] = in[s-1-i];
	  return out;
	}
	template cvec reverse(const cvec &v);
	template vec reverse(const vec &v);

//---------Conjugate
	cvec conj(const cvec &x) {
	  cvec temp(x.size());

	  for (int i = 0; i < x.size(); i++) {
		temp(i) = std::conj(x(i));
	  }
	  return temp;
	}

	cmat conj(const cmat &x) {
	  cmat temp(x.rows(), x.cols());

	  for (int i = 0; i < x.rows(); i++) {
		for (int j = 0; j < x.cols(); j++) {
		  temp(i, j) = std::conj(x(i, j));
		}
	  }

	  return temp;
	}

//-----------Getting real part of cvec as vec
	vec real(const cvec &data) {
	  vec temp(data.length());
	  for (int i = 0;i < data.length();i++)
	    temp[i] = data[i].real();
	  return temp;
	}
//-----------Getting imag part of cvec as vec
	vec imag(const cvec &data) {
	  vec temp(data.length());
	  for (int i = 0;i < data.length();i++)
		temp[i] = data[i].imag();
	  return temp;
	}

	vec abs(const cvec &data) {
	  vec temp(data.length());

	  for (int i = 0;i < data.length();i++)
	    temp[i] = std::abs(data[i]);

	  return temp;
	}


//--------Sum
	template<class T>
	T sum(const Vec<T> &v)
	{
	  T M = 0;
	  for (int i = 0;i < v.length();i++)
	    M += v[i];
	  return M;
	}

//-----Maximum value of vector
	template<class T>
	T max(const Vec<T> &v)
	{
	  T maxdata = v(0);
	  for (int i = 1; i < v.length(); i++)
	    if (v(i) > maxdata)
	      maxdata = v(i);
	  return maxdata;
	}
	//Template Initializations
	template int max(const Vec<int> &v);
	template double max(const Vec<double> &v);


//--------Mean Value
	double mean(const vec &v)
	{
	  return sum(v) / v.length();
	}

	std::complex<double> mean(const cvec &v)
	{
	  return sum(v) / double(v.size());
	}

	double mean(const svec &v)
	{
	  return (double)sum(v) / v.length();
	}

	double mean(const ivec &v)
	{
	  return (double)sum(v) / v.length();
	}
	//Not all template types here.. More for 'mat' etc, have been omitted. FUnction mean is from itpp/base/matfunc.h


//-------------Dec2bin
	bvec dec2bin(int length, int index)
	{
	  int i, bintemp = index;
	  bvec temp(length);

	  for (i = length - 1; i >= 0; i--) {
	    temp(i) = bin(bintemp & 1);
	    bintemp = (bintemp >> 1);
	  }
	  return temp;
	}

	bvec dec2bin(int index, bool msb_first)
	{
	  int length = int2bits(index);
	  int i, bintemp = index;
	  bvec temp(length);

	  for (i = length - 1; i >= 0; i--) {
	    temp(i) = bin(bintemp & 1);
	    bintemp = (bintemp >> 1);
	  }
	  if (msb_first) {
	    return temp;
	  }
	  else {
	    return reverse(temp);
	  }
	}

	void dec2bin(int index, bvec &v)
	{
	  int i, bintemp = index;
	  v.set_size(int2bits(index), false);

	  for (i = v.size() - 1; i >= 0; i--) {
	    v(i) = bin(bintemp & 1);
	    bintemp = (bintemp >> 1);
	  }
	}

	int bin2dec(const bvec &inbvec, bool msb_first)
	{
	  int i, temp = 0;
	  int sizebvec = inbvec.length();
	  if (msb_first) {
	    for (i = 0; i < sizebvec; i++) {
	      temp += pow(2,sizebvec - i - 1) * int(inbvec(i));
	    }
	  }
	  else {
	    for (i = 0; i < sizebvec; i++) {
	      temp += pow(2,i) * int(inbvec(i));
	    }
	  }
	  return temp;
	}

// ----------------------------------------------------------------------
// Instantiations
// ----------------------------------------------------------------------



	template cvec to_cvec(const bvec &v);
	template cvec to_cvec(const svec &v);
	template cvec to_cvec(const ivec &v);
	template cvec to_cvec(const vec &v);

	extern template vec zero_pad(const vec &v, int n);
	extern template cvec zero_pad(const cvec &v, int n);
	extern template ivec zero_pad(const ivec &v, int n);
	extern template svec zero_pad(const svec &v, int n);
	extern template bvec zero_pad(const bvec &v, int n);

} //trill

