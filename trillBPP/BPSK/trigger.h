/*
 * trigger.h
 *
 *  Created on: 27-May-2017
 *      Author: sid
 */

#ifndef TRIGGER_H_
#define TRIGGER_H_


#include<iostream>
//REmove the above

#include <trillBPP/vector/vec.h>
#include <trillBPP/vector/transforms.h>
#include <trillBPP/miscfunc.h>

#define Fs 44100
#define GammaMin 50
//#include <numeric>

using namespace trill;
using namespace std;


class Trigger {
public:
	double isFound(cvec input, bool solo_correlation=false);//For calculating gamma_value

	//---returns values of percentage for each input passed in result_array
    void isFound(cvec input, float *result_array);
    void windowCall(cvec input, int &chunkSize, int &windowSize, int *numbers, int *power_percentage);
};

#endif /* TRIGGER_H_ */
