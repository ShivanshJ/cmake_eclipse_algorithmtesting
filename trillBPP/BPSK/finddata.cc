//
//  Finddata.cpp
//  AudioQueue
//
//  Created by Shivansh on 6/22/17.
//
//

#include <trillBPP/BPSK/finddata.h>

FindData::FindData(){
	syncWave = "5.323768843796552151e-04,3.837572180392566801e-03,7.322273945476865872e-03,1.097951800429636596e-02,1.480158984477001650e-02,1.878005571146002667e-02,2.290579746878519674e-02,2.716905106424967811e-02,3.155944840118669187e-02,3.606606241083788744e-02,4.067745509502089518e-02,4.538172829331282854e-02,5.016657691269969105e-02,5.501934434309693756e-02,5.992707976913283496e-02,6.487659707720148938e-02,6.985453504706770622e-02,7.484741850935593133e-02,7.984172014408641438e-02,8.482392259110374533e-02,8.978058054081208827e-02,9.469838247302529710e-02,9.956421171320521679e-02,1.043652064784660882e-01,1.090888185909760222e-01,1.137228705433306897e-01,1.182556106093118897e-01,1.226757657040362937e-01,1.269725917098343815e-01,1.311359209981575458e-01,1.351562068933642724e-01,1.390245648412665180e-01,1.427328100637320907e-01,1.462734915003448544e-01,1.496399218589917202e-01,1.528262036191646023e-01,1.558272508546091895e-01,1.586388067655748191e-01,1.612574568352063420e-01,1.670465031226132380e-01,1.687363937900275834e-01,1.702004932773842349e-01,1.714421432684949953e-01,1.724657427332728921e-01,1.732767140246533055e-01,1.738814631015174750e-01,1.742873341146943533e-01,1.745025586280821162e-01,1.745361997803574206e-01,1.743980917244110640e-01,1.740987747113592610e-01,1.736494262135327427e-01,1.730617885060724559e-01,1.723480931494798529e-01,1.715209828355305499e-01,1.705934310762461104e-01,1.695786602299718948e-01,1.684900583699694421e-01,1.673410955091794716e-01,1.661452396999102132e-01,1.649158735290913169e-01,1.636662115283432639e-01,1.624092190136290359e-01,1.611575328612903646e-01,1.599233847163920863e-01,1.587185271150869659e-01,1.575541629855357895e-01,1.564408789717331161e-01,1.553885830015739322e-01,1.544064464947410564e-01,1.535028515776682700e-01,1.526853436421107546e-01,1.519605895508880367e-01,1.513343417593827900e-01,1.508114085845687624e-01,1.503956308149370924e-01,1.500898648149241454e-01,1.498959722365490188e-01,1.498148164092222845e-01,1.532121310095193867e-01,1.528189551297284010e-01,1.525074201117345707e-01,1.522778381756043353e-01,1.521298209450108740e-01,1.520622945992889385e-01,1.520735194093864040e-01,1.521611135173470408e-01,1.523220807926899401e-01,1.525528425737950056e-01,1.528492730783150844e-01,1.532067382438626335e-01,1.536201377388949074e-01,1.540839498639932359e-01,1.545922790457060492e-01,1.551389056089176044e-01,1.557173374994289816e-01,1.563208636161582399e-01,1.569426084021880441e-01,1.575755873358531400e-01,1.582127629572365468e-01,1.588471010618646151e-01,1.594716266920255809e-01,1.600794795572117457e-01,1.606639685182649213e-01,1.612186247754297530e-01,1.617372534081729929e-01,1.622139829245584641e-01,1.626433124899902827e-01,1.630201565192340718e-01,1.633398863316897764e-01,1.635983685878246718e-01,1.637920002444023526e-01,1.639177397875043740e-01,1.639731345252443129e-01,1.639563437463620044e-01,1.638661575764183143e-01,1.637020113899422813e-01,1.634639956644315939e-01,1.631528611904451143e-01,1.594041540077209551e-01,1.594877859068810355e-01,1.595322550753061064e-01,1.595375667900143513e-01,1.595041259749664098e-01,1.594327348003931888e-01,1.593245874625769387e-01,1.591812621690783802e-01,1.590047103702658116e-01,1.587972432936192446e-01,1.585615158525564561e-01,1.583005080163924772e-01,1.580175037424068063e-01,1.577160675848019444e-01,1.574000191085103695e-01,1.570734052482556653e-01,1.567404707650086604e-01,1.564056269628372742e-01,1.560734188391830757e-01,1.557484908506717725e-01,1.554355514847126363e-01,1.551393368342462997e-01,1.548645733791388923e-01,1.546159401826759927e-01,1.543980307156627707e-01,1.542153145234150535e-01,1.540720989527136486e-01,1.539724911564219756e-01,1.539203605930034857e-01,1.539193022365883401e-01,1.539726007105901573e-01,1.540831955541152876e-01,1.542536478256309185e-01,1.544861082425342536e-01,1.547822870484576729e-01,1.551434257923829108e-01,1.555702711949580597e-01,1.560630512678637116e-01,1.566214538416985103e-01,1.572446076467313991e-01,1.545652005057837897e-01,1.558490406262935113e-01,1.572192756749413167e-01,1.586693195991929484e-01,1.601917590310660278e-01,1.617783724248653132e-01,1.634201539978682727e-01,1.651073423862899625e-01,1.668294539021344391e-01,1.685753202504269577e-01,1.703331305409653273e-01,1.720904774042718577e-01,1.738344069980072848e-01,1.755514726678680248e-01,1.772277920060449929e-01,1.788491070307764164e-01,1.804008471925506452e-01,1.818681948961094197e-01,1.832361532127663728e-01,1.844896154446729053e-01,1.856134361916891573e-01,1.865925035624286243e-01,1.874118121639970769e-01,1.880565364997840438e-01,1.885121044017263325e-01,1.887642701223809227e-01,1.887991867132452761e-01,1.886034773188379199e-01,1.881643050211735668e-01,1.874694408763470932e-01,1.865073297939849883e-01,1.852671539212179530e-01,1.837388932055678270e-01,1.819133828255957153e-01,1.797823671942739288e-01,1.773385502577128070e-01,1.745756418309943048e-01,1.714883997333205290e-01,1.680726675063588249e-01,1.643254075224542532e-01,1.636105948863027792e-01,1.586596661179885681e-01,1.533473079210800616e-01,1.476785360706821815e-01,1.416597925974808769e-01,1.352989281393179866e-01,1.286051774263321512e-01,1.215891280337518116e-01,1.142626825741915042e-01,1.066390145379913362e-01,9.873251802553494050e-02,9.055875164940316302e-02,8.213437691642781757e-02,7.347709143001997234e-02,6.460555728140490728e-02,5.553932502438779784e-02,4.629875365188688974e-02,3.690492701354435023e-02,2.737956713213209048e-02,1.774494489211730919e-02,8.023788586557913799e-03,-1.760809181656591144e-03,-1.158548574134643863e-02,-2.142670323052779657e-02,-3.126084360233466475e-02,-4.106430325393316261e-02,-5.081358677827853970e-02,-6.048539934672662877e-02,-7.005673724161712301e-02,-7.950497607189450922e-02,-8.880795622150787494e-02,-9.794406509966038787e-02,-1.068923157838051752e-01,-1.156324216704727181e-01,-1.241448667754282004e-01,-1.324109713530925159e-01,-1.404129525354552155e-01,-1.481339797226780552e-01,-1.555582244909969880e-01,-1.626709048181896322e-01,-1.689530811652744380e-01,-1.743950316045831672e-01,-1.794962631679301868e-01,-1.842521594328393186e-01,-1.886592072088270344e-01,-1.927149751687558532e-01,-1.964180869208033697e-01,-1.997681887536683909e-01,-2.027659123205949321e-01,-2.054128325591593474e-01,-2.077114211733127602e-01,-2.096649960316987471e-01,-2.112776668616017928e-01,-2.125542776408514967e-01,-2.135003461104427180e-01,-2.141220008484026882e-01,-2.144259163604034513e-01,-2.144192466546828435e-01,-2.141095577778963088e-01,-2.135047597945077813e-01,-2.126130386951779472e-01,-2.114427887192925071e-01,-2.100025455733204593e-01,-2.083009210199101013e-01,-2.063465393029498829e-01,-2.041479758607660233e-01,-2.017136987637055123e-01,-1.990520132933994990e-01,-1.961710100592120587e-01,-1.930785170228478398e-01,-1.897820557749852477e-01,-1.862888023782497682e-01,-1.826055530590473819e-01,-1.787386949969134997e-01,-1.746941824243127550e-01,-1.704775182124751987e-01,-1.660937410800849556e-01,-1.615474185217246339e-01,-1.568426455121368912e-01,-1.519830490008853008e-01,-1.397348247263920373e-01,-1.346392453319159577e-01,-1.294611260895967841e-01,-1.242013478933490511e-01,-1.188599237173745410e-01,-1.134360473048539042e-01,-1.079281502906281470e-01,-1.023339673422655055e-01,-9.665060884326645563e-02,-9.087464058441552250e-02,-8.500216987484147946e-02,-7.902893743360361878e-02,-7.295041437591934241e-02,-6.676190356581028496e-02,-6.045864456932326103e-02,-5.403592140981531428e-02,-4.748917229933111162e-02,-4.081410049810518109e-02,-3.400678543780065455e-02,-2.706379323347431198e-02,-1.998228570447900532e-02,-1.276012702569871357e-02,-5.395987137629978253e-03,2.110558942968326912e-03,9.758936751199789178e-03,1.754748375526658799e-02,2.547336727458734268e-02,3.353250968234673668e-02,4.171952159846231689e-02,5.002764373224897337e-02,5.844869798246486725e-02,6.697304834626660597e-02,7.558957212819111815e-02,8.428564187598158730e-02,9.304711840228836739e-02,1.018583551803907278e-01,1.107022143285570914e-01,1.195600943219387313e-01,1.284119694934367228e-01,1.372364413063044408e-01,1.465160435991200438e-01,1.562239743091017596e-01,1.658241961538364784e-01,1.752849058660569115e-01,1.845734286922381950e-01,1.936563648446616159e-01,2.024997449098252800e-01,2.110691934450144580e-01,2.193300999302854049e-01,2.272477961828356652e-01,2.347877392851613898e-01,2.419156990279334452e-01,2.485979488235253165e-01,2.548014590069639929e-01,2.604940914080359216e-01,2.656447940516550865e-01,2.702237948236316800e-01,2.742027929258525343e-01,2.775551469387789183e-01,2.802560583101915137e-01,2.822827490973357500e-01,2.836146328051114551e-01,2.842334771856017683e-01,2.841235578942117312e-01,2.832718019345050742e-01,2.816679198678091467e-01,2.793045258142192178e-01,2.761772443287737189e-01,2.722848032999174994e-01,2.676291120866549633e-01,2.622153241856264860e-01,2.560518837993533414e-01,2.491505557616621735e-01,2.415264383653443481e-01,2.331979587299515544e-01,2.241868504437600218e-01,2.145181133128044870e-01,2.042199551509233102e-01,1.933237156473991414e-01,1.818637724524163890e-01,1.737485375950954536e-01,1.617474113973861316e-01,1.492664224940879469e-01,1.363485045693125508e-01,1.230391958274885261e-01,1.093864560371236722e-01,9.544046753333847166e-02,8.125342109557562309e-02,6.687928771600981648e-02,5.237357736812747050e-02,3.779308597310877121e-02,2.319563184349623594e-02,8.639782958584958303e-03,-5.815423506471930487e-03,-2.011076791610763928e-02,-3.418713964113658660e-02,-4.798583168087480755e-02,-6.144883683063676744e-02,-7.451914376000143281e-02,-8.714103135194856131e-02,-9.926035964728940897e-02,-1.108248557428052616e-01,-1.217843930051500101e-01,-1.320912619860884485e-01,-1.417004314575035506e-01,-1.505697980272291125e-01,-1.586604228485072876e-01,-1.659367539967277483e-01,-1.723668331566806933e-01,-1.779224853415405927e-01,-1.825794904507195004e-01,-1.863177355671951085e-01,-1.891213469953699489e-01,-1.909788011474299885e-01,-1.918830134989422120e-01,-1.918314049524338372e-01,-1.908259450702546323e-01,-1.888731717644650809e-01,-1.859841871610713182e-01,-1.821746294879188210e-01,-1.735935130986672736e-01,-1.675360699350629812e-01,-1.606678645202706257e-01,-1.530242328113674277e-01,-1.446442600631340225e-01,-1.355706042620009044e-01,-1.258493003617781325e-01,-1.155295462352389363e-01,-1.046634713604396860e-01,-9.330588936041060710e-02,-8.151403560893100142e-02,-6.934729120299339133e-02,-5.686689468368604644e-02,-4.413564296109803026e-02,-3.121758296504823990e-02,-1.817769560150817726e-02,-5.081573644167903733e-03,8.004904768592190079e-03,2.101590532937148634e-02,3.388596772566038889e-02,4.655033503464442446e-02,5.894528060817016968e-02,7.100843071332904599e-02,8.267908121011827149e-02,9.389850657294857017e-02,1.046102595978278815e-01,1.147604601812569070e-01,1.242980716102397715e-01,1.331751628649873731e-01,1.413471555066030394e-01,1.487730538008493752e-01,1.554156568156538909e-01,1.612417513238107447e-01,1.662222844428705437e-01,1.703325150509656938e-01,1.735521431296663253e-01,1.758654163023027350e-01,1.772612129578978735e-01,1.777331014761960037e-01,1.772793751976174670e-01,1.720319550420280341e-01,1.692692928676059472e-01,1.656405474499387798e-01,1.611650831958408026e-01,1.558665065302143049e-01,1.497725463015917580e-01,1.429149132261865462e-01,1.353291390750070711e-01,1.270543964165531947e-01,1.181332998314190835e-01,1.086116896141786359e-01,9.853839907169512080e-02,8.796500661493633633e-02,7.694557392298438658e-02,6.553637153277866445e-02,5.379559327574603977e-02,4.178306104252152431e-02,2.955992140906766805e-02,1.718833570137026992e-02,4.731165111277239876e-03,-7.748347497241660220e-03,-2.018689743431398981e-02,-3.252143801684816149e-02,-4.468949872179378557e-02,-5.662950000325404909e-02,-6.828106313481982492e-02,-7.958531346552905061e-02,-9.048517551359161470e-02,-1.009256583663053486e-01,-1.108541299072140635e-01,-1.202205784521481052e-01,-1.289778604441032817e-01,-1.370819329325188296e-01,-1.444920696449936148e-01,-1.511710595484191400e-01,-1.570853868913512086e-01,-1.622053918197068068e-01,-1.665054107629990177e-01,-1.699638958976876224e-01,-1.725635131072653927e-01,-1.781623258452819658e-01,-1.794809313706488829e-01,-1.798782772887842774e-01,-1.793521083519059311e-01,-1.779048289017567674e-01,-1.755434751827386342e-01,-1.722796633281065415e-01,-1.681295133814104947e-01,-1.631135498457416122e-01,-1.572565793805950907e-01,-1.505875463895755551e-01,-1.431393673610658057e-01,-1.349487449376884363e-01,-1.260559627982668629e-01,-1.165046625374742267e-01,-1.063416038228396782e-01,-9.561640919579496301e-02,-8.438129496248292227e-02,-7.269078969070026652e-02,-6.060144189123840841e-02,-4.817151851470811985e-02,-3.546069593836245759e-02,-2.252974515133833455e-02,-9.440212870892639163e-03,3.745899663478727271e-03,1.696645846666608448e-02,3.015951740937317105e-02,4.326363591358345362e-02,5.621819203615441296e-02,6.896368926899087981e-02,8.144205542780903750e-02,9.359693205405669736e-02,1.053739528157184857e-01,1.167210094621059235e-01,1.275885039649325425e-01,1.379295855624575740e-01,1.477003715147786334e-01,1.568601504758970477e-01,1.653715674913553979e-01,1.732007897384733597e-01,1.841887601093304960e-01,1.910384047976689814e-01,1.970905225142927097e-01,2.023243049757566525e-01,2.067232737428151834e-01,2.102753273327001016e-01,2.129727641080189360e-01,2.148122810097617408e-01,2.157949483384754563e-01,2.159261609221213574e-01,2.152155661406820741e-01,2.136769694054977686e-01,2.113282178148454260e-01,2.081910628257500617e-01,2.042910028947700629e-01,1.996571071468887237e-01,1.943218212311371706e-01,1.883207566135742528e-01,1.816924646423220269e-01,1.744781967950262136e-01,1.667216525860329146e-01,1.584687166683603765e-01,1.497671867139870605e-01,1.406664936948270883e-01,1.312174162158333823e-01,1.214717905709448709e-01,1.114822182019348518e-01,1.013017722397274167e-01,9.098370479746178963e-02,8.058115666463852578e-02,7.014687102230231230e-02,5.973291276060916505e-02,4.939039493262380176e-02,3.916921382214198777e-02,2.911779403911679207e-02,1.928284498436174180e-02,9.709129946079536264e-03,4.392490049462778254e-04,-8.486563167443386332e-03";
}


double FindData::biggest_peak(vec corr_op, const int &size){
    double big = corr_op[0];

    F(i,0,size) {
        if (corr_op[i] > big)
            big = corr_op[i];
    }
    cout<<"BIG :"<<big;
    big = big*(PERCENTAGE/100.0); //Anything above 45% Peak, make sure its 100.0
    return big;
}

vector<int> FindData::mark_every_frame(vec corr_op, const int &size){
    double big;
    vector<int> marked_array;
    big = biggest_peak(corr_op, size);
    F(i,0,size)
    if(corr_op[i] >= big){
        marked_array.push_back(i);
        i += FRAME_SIZE;
    }
    return marked_array;
}


void FindData::detect_distance_v4(vec maxIndices1, vec maxIndices2, int* result_array){
    const int size1 = maxIndices1.size();
    const int size2 = maxIndices2.size();
    const int *size;
    if(size1 >= size2) size = &size1;
    else size = &size2;
    F(i,0,3)
    result_array[i]=-1;

    int peak1_1, peak1_2;
    int peak2_1 , peak2_2;
    if(*size == size1){
        for (int i=0; i< *size; i++) {
            peak1_1 = maxIndices1[i];
            for (int j=0; j<*size; j++) {

                if(result_array[0]==-1){
                    peak1_2 = maxIndices1[j];
                    int disAbs1 = abs(peak1_1 - peak1_2);
                    disAbs1 = abs(peak1_1 - peak1_2);
                    disAbs1 = disAbs1 - MIN_DIS_BWT_PEAKS;
                    if( disAbs1 >= 0 && disAbs1 < MAX_DIS_BWT_PEAKS and disAbs1%PEAK_MOD_FACTOR == 0)
                        result_array[0] = disAbs1 + MIN_DIS_BWT_PEAKS;
                } //IF statement, for 1st chunk result

                if(i <size2 && j < size2){
                    peak2_2 = maxIndices2[j];
                    if (result_array[1] == -1){
                        peak2_1 = maxIndices2[i];
                        int disAbs2 = abs(peak2_1 - peak2_2);
                        disAbs2 = disAbs2 - MIN_DIS_BWT_PEAKS;
                        if (disAbs2 >= 0 && disAbs2 < MAX_DIS_BWT_PEAKS && disAbs2%PEAK_MOD_FACTOR == 0)
                            result_array[1] = disAbs2 + MIN_DIS_BWT_PEAKS;
                    }
                    // Chunk distance (12K - OTP)
                    int disAbs3 = abs(peak1_1 - peak2_2);
                    if (result_array[2] == -1 && disAbs3 < 28600){
                        //print peak1_1, peak2_2, disAbs3
                        disAbs3 = abs(disAbs3 - 28600) - MIN_DIS_BWT_PEAKS;
                        if (disAbs3 >= 0 && disAbs3 < MAX_DIS_BWT_PEAKS && (disAbs3%PEAK_MOD_FACTOR==0) ){
                            cout<< " dis "<<disAbs3<<" "<<(int)(disAbs3/PEAK_MOD_FACTOR)<<endl;
                        	result_array[2] = PEAK_MOD_FACTOR*(int)(disAbs3/PEAK_MOD_FACTOR) + MIN_DIS_BWT_PEAKS;
                        }
                    }
                }//IF i<size2

                if(result_array[2]!=-1 && result_array[1]!=-1 && result_array[0]!=-1){
                    return;
                }
            }//FOR j
        }//FOR i

    }//IF END
    else if(*size == size2){
        for (int i=0; i< *size; i++) {
            peak1_1 = maxIndices2[i];
            for (int j=0; j<*size; j++) {

                if(result_array[0]==-1){
                    peak1_2 = maxIndices2[j];
                    int disAbs1 = abs(peak1_1 - peak1_2);
                    disAbs1 = abs(peak1_1 - peak1_2);
                    disAbs1 = disAbs1 - MIN_DIS_BWT_PEAKS;
                    if( disAbs1 >= 0 && disAbs1 < MAX_DIS_BWT_PEAKS and disAbs1%PEAK_MOD_FACTOR == 0)
                        result_array[0] = disAbs1 + MIN_DIS_BWT_PEAKS;
                } //IF statement, for 1st chunk result

                if(i <size1 && j < size1){
                    peak2_2 = maxIndices1[j];
                    if (result_array[1] == -1){
                        peak2_1 = maxIndices1[i];
                        int disAbs2 = abs(peak2_1 - peak2_2);
                        disAbs2 = disAbs2 - MIN_DIS_BWT_PEAKS;
                        if (disAbs2 >= 0 && disAbs2 < MAX_DIS_BWT_PEAKS && disAbs2%PEAK_MOD_FACTOR == 0)
                            result_array[1] = disAbs2 + MIN_DIS_BWT_PEAKS;
                    }
                    // Chunk distance (12K - OTP)
                    int disAbs3 = abs(peak1_1 - peak2_2);
                    if (result_array[2] == -1 && disAbs3 < 28600){
                        //print peak1_1, peak2_2, disAbs3
                        disAbs3 = abs(28600 - disAbs3) - MIN_DIS_BWT_PEAKS;
                        if (disAbs3 >= 0 && disAbs3 < MAX_DIS_BWT_PEAKS && (disAbs3%PEAK_MOD_FACTOR==0) )
                            result_array[2] = disAbs3 + MIN_DIS_BWT_PEAKS;
                    }
                }//IF i<size1

                if(result_array[2]!=-1 && result_array[1]!=-1 && result_array[0]!=-1){
                    return;
                }
            }//FOR j
        }//FOR i

    }//ELSE IF

}

ivec FindData::getPeaks_array(vec corrOut, const int &separation) {
    vector<int> marked_array;
    int size = corrOut.size();
    marked_array = mark_every_frame(corrOut,size);
    int no_of_frames = int( marked_array.size() ); //Total marked points(having peak data) in the whole sample

    cout<<"\nMARKED ARRAY: ";
    F(i,0,no_of_frames){
        cout<<marked_array[i]<<" ";
    }

    cout<<endl;
    int FINAL_ARRAY_SIZE = INDICES_FOR_PEAKS*no_of_frames;
    //@sorted_mag : contains the magnitude of the top @INDICES_FOR_PEAKS
    //@sorted_marked_index[][]: contains the 'ORIGINAL' index of the top @INDICES_FOR_PEAKS, and a unique ID
    vec sorted_mag;
    ivec sorted_marked_index;
    F(i,0,no_of_frames){
        //#Getting new array of size FRAME_SIZE + BACK_SECURE_FRAME
        int framesize_macro;
        //Incase adding frame_size is more than the array_size
        if( (marked_array[i] + FRAME_SIZE) > size)
            framesize_macro = -(marked_array[i]) + (size-1);
        else framesize_macro = FRAME_SIZE;

        vec new_array = corrOut.get(marked_array[i]-BACK_SECURE_FRAME, marked_array[i] + framesize_macro);
        int resIndexLen = (marked_array[i] + framesize_macro) - (marked_array[i]-BACK_SECURE_FRAME) + 1;
        if(resIndexLen<11)
            continue;
        //pICK OUT K elements,i.e, INDICES_FOR_PEAKS
        ivec resIndex = sort_index(new_array);
        resIndex = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);
        F(j,0,INDICES_FOR_PEAKS){
            sorted_mag = concat(sorted_mag, new_array[ resIndex[j] ]);  //Magnitude of the top-=peaks
            resIndex[j] = resIndex[j] + (marked_array[i]- BACK_SECURE_FRAME) + separation; //Adding @marked_array[], to get original index
        }
        sorted_marked_index = concat(sorted_marked_index, resIndex);
    }//FOR
    FINAL_ARRAY_SIZE = sorted_marked_index.size();
    cout<<"FINAL ARRAY SIZE : "<<FINAL_ARRAY_SIZE<<endl;
    F(i,0,FINAL_ARRAY_SIZE)
    	printf("%d : %.20f \n",sorted_marked_index[i], sorted_mag[i]);
    cout<<endl;

    //@sorted_mag_index : Containes indexes of the magnitude after sorting
    ivec sorted_mag_index;
    sorted_mag_index = sort_index(sorted_mag);

    int new_FINAL_ARRAY_SIZE = FINAL_ARRAY_SIZE ;
    if (FINAL_ARRAY_SIZE > 20) new_FINAL_ARRAY_SIZE = 20;

    ivec maxIndices(new_FINAL_ARRAY_SIZE);  //Stores the sorted_indexes according to their magnitude of the @sort_type going on
    //After sorting magnitude index, putting the ORIGINAL index values of where that magnitude occurs
    F(i,0,new_FINAL_ARRAY_SIZE){
        maxIndices[i] = sorted_marked_index[ sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i] ];
    }
    cout<<endl;
    return maxIndices;
}


vec FindData::apply(vec inputWave) {
    Correlate corr;
//    cout<<syncWave;
    vec resCorr = corr.apply(inputWave, syncWave);
    const int size = resCorr.size();

    return resCorr;
}


int* FindData::applyTrillAlgorithm_v3(vec corrOut, int *result_array) {
    //Getting correlation first
    const int size = corrOut.size();
    cout<<"\nSize"<<size<<endl;
    //..

    vector<int> marked_array;
    marked_array = mark_every_frame(corrOut,size);
    int no_of_frames = int( marked_array.size() ); //Total marked points(having peak data) in the whole sample
    if(no_of_frames > EXPECTED_PEAK_PAIR){
        F(i,0,3)
        result_array[i] = -1;
        return result_array;
    }
    cout<<"\nMARKED ARRAY: ";
    F(i,0,no_of_frames){
        cout<<marked_array[i]<<" ";
    }

    cout<<endl;
    int FINAL_ARRAY_SIZE = INDICES_FOR_PEAKS*no_of_frames;
    //@sorted_mag : contains the magnitude of the top @INDICES_FOR_PEAKS
    //@sorted_marked_index[][]: contains the 'ORIGINAL' index of the top @INDICES_FOR_PEAKS, and a unique ID
    vec sorted_mag;
    int sorted_marked_index[FINAL_ARRAY_SIZE][2];
    F(i,0,no_of_frames){
        //#Getting new array of size FRAME_SIZE + BACK_SECURE_FRAME
        int framesize_macro;

        //Incase adding frame_size is more than the array_size
        if( (marked_array[i] + FRAME_SIZE) > size)
            framesize_macro = -(marked_array[i]) + (size-1);
        else framesize_macro = FRAME_SIZE;

        vec new_array = corrOut.get(marked_array[i]-BACK_SECURE_FRAME, marked_array[i] + framesize_macro);
        int resIndexLen = (marked_array[i] + framesize_macro) - (marked_array[i]-BACK_SECURE_FRAME) + 1;
        if(resIndexLen<11)
            continue;

        //pICK OUT K elements,i.e, INDICES_FOR_PEAKS
        ivec resIndex = sort_index(new_array);
        resIndex = resIndex.get(resIndexLen-INDICES_FOR_PEAKS, resIndexLen-1);

        F(j,0,INDICES_FOR_PEAKS){
            sorted_mag = concat(sorted_mag, new_array[ resIndex[j] ]);  //Magnitude of the top-=peaks
            resIndex[j] = resIndex[j] + (marked_array[i]- BACK_SECURE_FRAME); //Adding @marked_array[], to get original index
            //CONCAT all top peak indexes, to sorted_marked_index
            sorted_marked_index[(i*INDICES_FOR_PEAKS) + j][0] = resIndex[j];
            sorted_marked_index[(i*INDICES_FOR_PEAKS) + j][1] = (i*INDICES_FOR_PEAKS) + j;
        }
    }
    FINAL_ARRAY_SIZE = sorted_mag.size();
    cout<<endl;
    //    __android_log_print(ANDROID_LOG_ERROR, "res", "test int = %d", 1);
    F(i,0,FINAL_ARRAY_SIZE)
    {cout<<sorted_marked_index[i][0]<<" "<<sorted_marked_index[i][1]<<" "<<sorted_mag[i]<<endl;
    }

    //@sorted_mag_index : Containes indexes of the magnitude after sorting
    ivec sorted_mag_index;
    sorted_mag_index = sort_index(sorted_mag);

    int sort_type;          //Running for SORT5, SORT7, SORT10
    int sort_counter = 0;
    int cache[FINAL_ARRAY_SIZE][FINAL_ARRAY_SIZE];  //Cache to store already subtracted peaks for different sort_types
    F(i,0,FINAL_ARRAY_SIZE) //Initialize cache value with -1:
    F(j,0,FINAL_ARRAY_SIZE)
    cache[i][j] = -5;


    while(sort_counter<3){
        if(sort_counter == 0) sort_type = 5;
        else if(sort_counter == 1) sort_type = 7;
        else if(sort_counter == 2) sort_type = 10;
        else break;

        int maxIndices[sort_type*no_of_frames][2];  //Stores the sorted_indexes according to their magnitude of the @sort_type going on
        int max_size;
        max_size = sort_type*no_of_frames;
        cout<<endl<<"++"<<endl;
        //After sorting magnitude index, putting the ORIGINAL index values of where that magnitude occurs
        int ct_maxInd = 0;
        F(i,0,FINAL_ARRAY_SIZE){
            //Choose 'i' such that top-x (5,7,10) peaks are fit into it. Like for top5, sorted_marked_index[(9-5):9], etc are chosen.
            if( sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i]%INDICES_FOR_PEAKS >= (INDICES_FOR_PEAKS - sort_type) ){
                maxIndices[ct_maxInd][0] = sorted_marked_index[ sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i] ][0];
                maxIndices[ct_maxInd][1] = sorted_marked_index[ sorted_mag_index[FINAL_ARRAY_SIZE - 1 - i] ][1];
                cout<<maxIndices[ct_maxInd][0]<<"^"<<maxIndices[ct_maxInd][1]<<" ";
                ct_maxInd++;
            }
        }


        //#FOR SHORTBUFFER
        int PEAK_detect_flag = 0;
        cout<<"-----------------------------------------";
        int peak1Selected;
        int peak2Selected;
        for (int i=0;i< max_size; i++) {
            peak1Selected = maxIndices[i][0];
            //        INDICES_FOR_PEAKS* floor((i+INDICES_FOR_PEAKS)/INDICES_FOR_PEAKS)
            for (int j= 0 ; j < max_size; j++) {
                peak2Selected = maxIndices[j][0];
                int absDistance = abs(peak1Selected - peak2Selected);
//                printf("Abs ij %d %d::%d ",i,j,absDistance);
                if (absDistance > 3500 && absDistance < 9800) {
                    int FinalDistance = (absDistance - 3500 );
                    if ( FinalDistance % PEAK_MOD_FACTOR < 1) {
                        FinalDistance = FinalDistance - (FinalDistance % PEAK_MOD_FACTOR);
                        result_array[sort_counter] = FinalDistance + 3500;
                        PEAK_detect_flag = 1;
                        break;
                    }
                    else if (FinalDistance % PEAK_MOD_FACTOR > PEAK_MOD_FACTOR - 1) {
                        FinalDistance = FinalDistance + (PEAK_MOD_FACTOR - (FinalDistance % PEAK_MOD_FACTOR));
                        result_array[sort_counter] =  FinalDistance + 3500;
                        PEAK_detect_flag = 1;
                        break;
                    }
                }
                if(PEAK_detect_flag == 1)
                    break;
            }
            if(PEAK_detect_flag == 1)
                break;
        }

        if(PEAK_detect_flag == 0)
            result_array[sort_counter] = -5;

        sort_counter++;
    }//While loop

    cout<<endl;
    return result_array;
}

