/*
 * BPSK.h
 *
 *  Created on: 09-Mar-2018
 *      Author: shivujagga
 */

#ifndef TRILLBPP_BPSK_BPSK_H_
#define TRILLBPP_BPSK_BPSK_H_

//remove
#include<iostream>
#include<iomanip>
using namespace std;

#include <trillBPP/vector/vec.h>
#include <trillBPP/BPSK/modulation.h>
#include <trillBPP/BPSK/finddata.h>
#include <trillBPP/BPSK/correlate.h>
#include <trillBPP/miscfunc.h>

//...Configuration
#define modulation_type "BPSK" //choose b/w BPSK or QPSK
#define LENGTH_OF_DATA 500 //number of bits being sent, multiply this by 40 to get number of samples
#define PEAK_SAMPLES 28000 //number of samples for correlation to find the Barker peaks

#define FS 44100
#define Fsync 17000
#define Fdata 18000
#define AMP_DATA 0.1
#define roll_off 0.1
#define SAMPLES_PER_SYMBOL 40
#define FRAMES_IN_RRC 400   //it is the 'frames' variable name in Python
#define OV_FACT 40
#define SPAN 10

#define DIST_BW_PEAKS 12520
#define DIST_PEAK_DATA 16280	//distance_of_data

using namespace trill;

class BPSK{
	vec rrc_wave;

public:
	BPSK();
	//Note: ov_fact was written as upsampling factor in Python Code
	template<typename T>
	T demodulate_BPSK(vec input, int fc=18000, int ov_fact=40, int span=10, int phi=0);

	vec demodulate_BPSK_RLS(vec &input, int fc=Fdata, int ov_fact=OV_FACT, int span=SPAN, int phi=0);

	//Does Peak detection and then sends for demodulation: returns final_bits
	//Note Input format : Trigger, + 24576 -> Peaks at x distance , + 12280 -> data start , where data length is 8038
	vec trillDecoder(vec &input);
	vec trillDecoder_RLS(vec input); //, double delta=1000, double lamda=0.999, int no_taps=2, int point_selection=2
};


#endif /* TRILLBPP_BPSK_BPSK_H_ */
