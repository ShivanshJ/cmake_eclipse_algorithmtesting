/*
 * miscfunc.h
 *
 *  Created on: 29-Sep-2017
 *      Author: shivujagga
 */

#ifndef TRILLBPP_MISCFUNC_H_
#define TRILLBPP_MISCFUNC_H_

#include <trillBPP/vector/vec.h>
#include <math.h> //Just for pow in BPSK.cc
#include <iostream> //For std::string's to_str function


namespace trill{

std::complex<double> elem_mult_sum_complex(const cvec &m1, const cvec &m2);	//Dot product of complex vectors

//-----CMAT operations
	ITPP_EXPORT cmat operator+(const double &s, const cmat &m);
	ITPP_EXPORT cmat operator/(const cmat &m, const double &s);

//-----Convert anything to string
	template <typename T>
	std::string to_str(const T &i);

//-----Reverse the input vector
	template<class T>
	Vec<T> reverse(const Vec<T> &in);

//----Conjugate of complex number
	cvec conj(const cvec &x);
	cmat conj(const cmat &x);

//----! The nearest larger integer
	inline int ceil_i(double x) { return static_cast<int>(std::ceil(x)); }

//-----Converts to cvec
	//\relatesalso Vec
	//\brief Converts a Vec<T> to cvec
	template <class T>
	cvec to_cvec(const Vec<T> &v) {
	  cvec temp(v.length());
	  for (int i = 0; i < v.length(); ++i) {
	    temp(i) = std::complex<double>(static_cast<double>(v(i)), 0.0);
	  }
	  return temp;
	}
	//! \cond
	template<> inline
	cvec to_cvec(const cvec& v)
	{
	  return v;
	}
	//! \endcond

//----Getting real & imagpart of cvec as vec
	vec real(const cvec &data);
	vec imag(const cvec &data);

//----Absolute value of cvec
	vec abs(const cvec &data);

//----Zero padding
	//! Zero-pad a vector to size n
	template<class T>
	Vec<T> zero_pad(const Vec<T> &v, int n)
	{
	  it_assert(n >= v.size(), "zero_pad() cannot shrink the vector!");
	  Vec<T> v2(n);
	  v2.set_subvector(0, v);
	  if (n > v.size())
	    v2.set_subvector(v.size(), n - 1, T(0));

	  return v2;
	}

//-----Sum
	//! Sum of all elements in the vector
	template<class T>
	T sum(const Vec<T> &v);

//-----Maximum value of vector
	template<class T>
	T max(const Vec<T> &v);

//-----Mean Values
	double mean(const vec &v);
	std::complex<double> mean(const cvec &v);
	double mean(const svec &v);
	double mean(const ivec &v);
	double mean(const mat &m);
	std::complex<double> mean(const cmat &m);
	double mean(const smat &m);
	double mean(const imat &m);

//----levels2bit
	inline int levels2bits(int n) {
	  it_assert(n >= 0, "int2bits(): Improper argument value");
	  if (n == 0) return 1;
	  int b = 0;
	  while (n) { n >>= 1;
		++b;
	  }
	  return b;
	}

//----------int2bits
	//! Calculate the number of bits needed to represent an integer \c n
	inline int int2bits(int n) {
	  it_assert(n >= 0, "int2bits(): Improper argument value");
	  if (n == 0)
		return 1;
	  int b = 0;
	  while (n) {
		n >>= 1;
		++b;
	  }
	  return b;
	}

//-----Dec2Bin and Bin2Dec
		//brief Convert a decimal int \a index to bvec using \a length bits in the representation
	ITPP_EXPORT bvec dec2bin(int length, int index);
		//brief Convert a decimal int \a index to bvec. Value returned in \a v.
	ITPP_EXPORT void dec2bin(int index, bvec &v);
		//brief Convert a decimal int \a index to bvec with the first bit as MSB if \a msb_first == true
	ITPP_EXPORT bvec dec2bin(int index, bool msb_first = true);
		//brief Convert a bvec to decimal int with the first bit as MSB if \a msb_first == true
	ITPP_EXPORT int bin2dec(const bvec &inbvec, bool msb_first = true);

} //namespace trill

#endif /* TRILLBPP_MISCFUNC_H_ */
