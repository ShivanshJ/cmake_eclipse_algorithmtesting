
ALGORITHM V3 included
-------------------------

1. main.cpp is accomodated to ALgorithm V3
2. Algorithm_V4 usage in main.cpp is commented out


------Changes in the following for speed improvement in Android. Currently taking 45 sec
1. CARRIER_DEMOD : concat //removal



Note : concat takes O(n^2) time.
Better way to do it is as follows : 

	string str;
	for(int i=0; i<num_items; i++){
		char data[30];
		sprintf(data, " %2f", buf[i]);
		str.append(data);
	}
	cvec itpp_vec = str;
