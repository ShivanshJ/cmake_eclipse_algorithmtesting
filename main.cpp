/*
 * main.cpp
 *
 *  Created on: 16-Aug-2017
 *      Author: shivujagga
 */



/////////PROGRAM WRITTEN USING LIBSNDFILE TO OPEN SOUND FILES
////DONT FORGET TO INSTALL AND INCLUDE THE LIBRARY IN ECLIPSE PROPERTIES OF PROJECT

#include "fftw3.h"
#include <trillBPP/BPSK/modulation.h>
#include <trillBPP/vector/vec.h>
#include <trillBPP/BPSK/finddata.h>
#include <trillBPP/BPSK/trigger.h>
#include <trillBPP/BPSK/BPSK.h>

#include <trillBPP/BPSK/modulation.h>
#include <trillBPP/comm/reedsolomon.h>

#include <iostream>
#include <iterator> //for ostream_iterator
#include <stdio.h>
#include <stdlib.h>
#include <sndfile.h>

#include <fstream>	//For JSON file
#include <jsoncpp/json/json.h>

#include <trillBPP/miscfunc.h>

//#define file_path "/home/shivujagga/Testing files/2Peaks/testfiles/default_37.wav"
//---------- Android
//#define file_path "/home/shivujagga/Softwares/Server/testingframework/2peakTest/audio/default_5.wav"
//---------- Iphone
//#define file_path "/home/shivujagga/Softwares/Server/testingframework/Android/audio_test_1024_2048/2048/0_meter38.wav"

#define file_path "/home/shivujagga/Documents/temp.wav"					  //For RLS
#define text_file_path "/home/shivujagga/Documents/data_raw_bpsk.json" //For RLS test

using namespace std;
using namespace trill;

#define SEPARATION 28000
#define TOTAL_CHUNKS 3

//---------------------------
//---BOOST Python
//--------------------------
//#include <boost/python.hpp>
//void format(string &val) {
//	replace( val.begin(), val.end(), ' ', ',');
//	replace( val.begin(), val.end(), '[', ' ');
//	replace( val.begin(), val.end(), ']', ' ');
//}
//
//template <typename T>
//std::string to_str(const T &i)
//{
//  std::ostringstream ss;
//  ss.precision(8);
//  ss.setf(std::ostringstream::scientific, std::ostringstream::floatfield);
//  ss << i;
//  return ss.str();
//}
//
//string demod(string buf) {
//	int num_items=10;
//	BPSK bP;
//	vec answer = bP.trillDecoder_RLS(buf);
//	cout<<"Answer Size :"<<answer.size()<<endl;
//	string val = to_str(answer);
//	format(val);
//	return val;
//}
//
//BOOST_PYTHON_MODULE(c_RLS)
//{
//		using namespace boost::python;
//		def("demod", demod);
//}

//-------------------------------------



int main(){
	cout<<"--------------Algorithm Welcomes you:"<<endl;

/*----Libsnd  Read file*/
	char        *infilename;
	SNDFILE     *file = NULL;

	SF_INFO     sfinfo;
	int num_channels;
	int num, num_items;
	double *buf;
	int frame, samplerate, ch;
	int i, j;
	FILE        *outfile = NULL;
	//Read the file, into buffer
	file = sf_open(file_path, SFM_READ, &sfinfo);
	/* Print some of the info, and figure out how much data to read. */
	frame = sfinfo.frames;
	samplerate = sfinfo.samplerate;
	ch = sfinfo.channels;
	printf("frames=%d\n", frame);
	printf("samplerate=%d\n", samplerate);
	printf("channels=%d\n", ch);
	num_items = frame * ch;
	printf("num_items=%d\n", num_items);
	//Allocate space for the data to be read, then read it
	buf = (double *)malloc(num_items * sizeof(double));
	num = sf_read_double(file, buf, num_items);
	printf("num=%d\n", num);
	sf_close(file);

/*----- End Libsnd*/


/*Starting normal stuff after reading in FINDDATA*/
//###Enable the two lines if instead of reading you have the "vec" part of the data
//  svec buf ="";
//	int num_items = buf.size();

//	vec divided_storage[TOTAL_CHUNKS];
//	//Dividing data
//	int flag = -1;
//	F(i,0, num_items){
//		if(i%SEPARATION==0){
//
//			flag++;
//		}
//		if(flag >=TOTAL_CHUNKS)
//		{
//			break;
//		}
//		char data[30];
//		sprintf(data, "%2f", buf[i]);
//		vec a = data;
//		divided_storage[flag] = concat(divided_storage[flag], a ) ;
//	}


//---------------------For WindowCall ,Frequency algo
//	Trigger tg;
//	cvec input;
//	int chunkSize = 2048;
//	int windowSize = 1024;
//	int ratio = chunkSize/windowSize;
//	F(i,1, num_items){
//		char data[30];
//		sprintf(data, "%2f", buf[i]);
//		cvec a = data;
//		input = concat(input, a ) ; //Going for window call
//
//		int array[ratio], power[ratio];
//		if(i%chunkSize == 0){
//			tg.windowCall(input,chunkSize,windowSize,array,power);
//			for(int k=0; k<ratio; k++)
//				cout<<array[k]<<" ";
//			cout<<endl;
//			input= ""; //Making input NULL
//		}
//	}


//---------------------For BPSK.h ,Decoding

	//Scipy python values from JSON
	ifstream text_file1(text_file_path);
	Json::Reader reader1;
	Json::Value obj1;
	reader1.parse(text_file1, obj1);     // Reader can also read strings
	vec JSON_input = obj1["data_raw"].asString();

	//Conversion of buf -> stringstream -> itpp vec
	std::stringstream result;
	std::copy(buf, buf+num_items, std::ostream_iterator<double>(result, ","));
	vec input = result.str();

	//TRILLDECODER - send input to the function
	BPSK bP;
	vec answer = bP.trillDecoder_RLS(JSON_input);
	cout<<"Answer Size :"<<answer.size()<<endl;
	for(int i=0; i<answer.size();i++)
		cout<<answer[i]<<",";
	cout<<"---"<<endl;

	//--error Check
	ifstream text_file(text_file_path);
	Json::Reader reader;
	Json::Value obj;
	reader.parse(text_file, obj);     // Reader can also read strings
	vec data_complete = obj["data_final_bits"].asString();
	int error_ct = 0;
	vector<int> index_error;
	for(int i=0; i<500; i++){
		if(answer[i]!=data_complete[i]){
			error_ct+=1;
			index_error.push_back(i);
			}
		cout<<data_complete[i]<<" ";
	}
	cout<<endl<<"Error_count: "<<error_ct<<endl;
	for(int i=0; i<index_error.size();i++)
		cout<<index_error[i]<<" ";


//------------------Reed solomon block code
//	  int m, t, n, k, q, NumBits, NumCodeWords;
//	  double p;
//	  bvec uncoded_bits= "1 1 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 0";
//	  bvec coded_bits, received_bits, decoded_bits;
//	  //Set parameters:
//	  p = 0.01;             //BSC Error probability
//	  m = 3;               //Reed-Solomon parameter m
//	  t = 2;                //Reed-Solomon parameter t
//	  cout << "Number of Reed-Solomon code-words to simulate: " <<  NumCodeWords << endl;
//	  cout << "BSC Error probability : " << p << endl;
//	  cout << "RS m: " << m << endl;
//	  cout << "RS t: " << t << endl;
//	  //Classes:
//	  Reed_Solomon reed_solomon(m, t);
//	  //Calculate parameters for the Reed-Solomon Code:
//	  n = static_cast<int>(pow(2.0, m) - 1 );
//	  k = static_cast<int>(pow(2.0, m) ) - 1 - 2 * t;
//	  q = static_cast<int>(pow(2.0, m) );
//	  cout << "Simulating an Reed-Solomon code with the following parameters:" << endl;
//	  cout << "n = " << n << endl;
//	  cout << "k = " << k << endl;
//	  cout << "q = " << q << endl;
//	  cout<<"Uncoded bits:"<<uncoded_bits.size()<<" :"<<uncoded_bits<<endl;
//	  coded_bits = reed_solomon.encode(uncoded_bits);
//	  cout<<"C bits:"<<coded_bits.size()<<" :"<<coded_bits<<endl;
//
//	  for(int i = 0; i<2; i++)
//		  if(coded_bits[i]==1)
//			  coded_bits[i]=0;
//		  else coded_bits[i]=1;
//	  for(int i = 17; i<21; i++)
//		  if(coded_bits[i]==1)
//			  coded_bits[i]=0;
//		  else coded_bits[i]=1;
//	  for(int i = 30; i<39; i++)
//		  if(coded_bits[i]==1)
//			  coded_bits[i]=0;
//		  else coded_bits[i]=1;
//
//	  cout<<"Erroneous Coded bits :";
//	  cout<<endl<<coded_bits.size()<<endl;
//	  for(int i = 0; i<coded_bits.size(); i++)
//		  cout<<coded_bits[i]<<" ";
//	  cout<<endl;
//	  decoded_bits = reed_solomon.decode(coded_bits);
//	  cout<<"\nDecoded Bits : "<<decoded_bits<<endl;
//-----------------------------------------------------------------------

	cout<<endl;
	cout<<"------ NOTE: The values if decoding original file  should be upto 8 precision";
	return 0;
}



